/**
 * A module with a single function using http.get() to request weather for given city.
 */
const http = require('http')

const api = {
  get: async function get(city) {
    return new Promise(function (resolve, reject) {
      const url = 'http://api.openweathermap.org/data/2.5/weather?q=' + city + '&units=metric&apikey=c184205bc1fcbcdc42c4b37ccf710de3'
      const req = http.get(url, (res) => {
        let body = '';
        res.on('data', function (chunk) { body += chunk; }); // On getting repsonse data, do this
        res.on('end', function () {         // when done getting response data, do this
          try {
            const o = JSON.parse(body)
            const s = 'In ' + o.name + ', temp is ' + o.main.temp + ' degrees C.'
            console.log(s)
            resolve(s)
          }
          catch (err) {
            reject('invalid city')
          }

        })
      })
      req.on('error', (err) => reject(err.message))
    })
  }
}

module.exports = api
